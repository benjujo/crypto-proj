# Proyecto para CC5301 - Introducción a la Criptografía Moderna
## Autenticación mediante une esquema de reencriptación homomórfica en proxies

El proyecto en sí se encuentra en la carpeta `second_try`, que para poder ejecutar el código de `example.py` es necesaria la librería de [proxy-re-demo](https://github.com/nebula-genomics/proxy-re-demo).
Para poder correrla es necesario parchar la librería umbral, agregando ciertas líneas al archivo `umbral/utils.py`:

Al inicio del archivo:
```python
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
```
Al final, después de la función `poly_eval`:
```python
def kdf(ecpoint: Point, key_length: int) -> bytes:
    data = ecpoint.to_bytes(is_compressed=True)

    return HKDF(
        algorithm=hashes.BLAKE2b(64),
        length=key_length,
        salt=None,
        info=None,
        backend=default_backend()
    ).derive(data)
```