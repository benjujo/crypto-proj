import random
from umbral import pre, keys, config, signing

#config.set_default_curve()

class Service():
    def __init__(self):
        # Generate Umbral keys for Service.
        self.private_key = keys.UmbralPrivateKey.gen_key()
        self.public_key = self.private_key.get_pubkey()

    def decrypt(self, ciphertext, capsule):
        cleartext = pre.decrypt(ciphertext=ciphertext,
                                capsule=capsule,
                                decrypting_key=self.private_key)
        return cleartext

    def get_pubkey(self):
        return self.public_key

    def get_kfrags(self, kfrags, user):
        rkfrags = random.sample(kfrags, 10)
        

