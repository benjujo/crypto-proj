import random
from umbral import pre, keys, config, signing

config.set_default_curve()

class User():
    def __init__(self):
        # Generate Umbral keys for User.
        self.private_key = keys.UmbralPrivateKey.gen_key()
        self.public_key = self.private_key.get_pubkey()

        self.signing_key = keys.UmbralPrivateKey.gen_key()
        self.verifying_key = self.signing_key.get_pubkey()
        self.signer = signing.Signer(private_key=self.signing_key)

    def self_encrypt(self, message):
        ciphertext, capsule = pre.encrypt(self.public_key, message)
        return ciphertext, capsule

    def self_decrypt(self, ciphertext, capsule):
        cleartext = pre.decrypt(ciphertext=ciphertext,
                                capsule=capsule,
                                decrypting_key=self.private_key)
        return cleartext

    def reencrypt_key_frags(self, rec_pubkey):
        kfrags = pre.generate_kfrags(delegating_privkey=self.private_key,
                                     signer=self.signer,
                                     receiving_pubkey=rec_pubkey,
                                     threshold=10,
                                     N=20)
        return kfrags

