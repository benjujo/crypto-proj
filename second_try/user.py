import functools
import random

from proxyre_demo import encrypt, keys, proxy_simulate, mapping

class User():
    def __init__(self):
        self.public_key, self.private_key = proxy_simulate.generate_collective_public_key()
        
        self.cipher = None
        self.last_cipher = None

    def self_encrypt(self, message):
        key_point, ciphertext = encrypt.encrypt(message)
        return key_point, ciphertext

    def homo_encrypt(self, message):
        msg_int = int.from_bytes(message, byteorder='big')
        point = mapping.int_to_point(msg_int)
        cipher = encrypt.el_gamal_encrypt(point, self.public_key)
        return cipher

    def add_data(self, message):
        new_cipher = self.homo_encrypt(message)
        self.last_cipher = self.cipher
        if self.cipher is not None:
            self.cipher = self.cipher[0] + new_cipher[0], self.cipher[1] + new_cipher[1]
        else:
            self.cipher = new_cipher

    def re_user_cipher(self, rec_pubkey):
        return proxy_simulate.re_encrypt(self.cipher,
                                         self.private_key,
                                         rec_pubkey)
    
            
