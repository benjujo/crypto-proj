from proxyre_demo import encrypt, keys

class Service():
    def __init__(self):
        self.private_key, self.public_key = keys.gen_key_pair()

    def get_pubkey(self):
        return self.public_key

    def decrypt_key_point(self, re_user_cipher):
        return encrypt.el_gamal_decrypt(re_user_cipher, self.private_key)

def decrypt(ciphertext, decrypted_key_point):
    return encrypt.decrypt(ciphertext, decrypted_key_point)

    
