from proxyre_demo import encrypt, keys, proxy_simulate, mapping

class Proxy():
    def __init__(self):
        self.collective_pub, self.server_priv_keys = proxy_simulate.generate_collective_public_key()

    def user_key_cipher(self, key_point):
        return encrypt.el_gamal_encrypt(key_point, self.collective_pub)

    def re_user_cipher(self, user_key_cipher, rec_pubkey):
        return proxy_simulate.re_encrypt(user_key_cipher,
                                         self.server_priv_keys,
                                         rec_pubkey)
    
