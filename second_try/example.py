from user import *
#from proxy import *
from service import *

'''
alice = User()
#proxy = Proxy()
service = Service()


kp, cp = alice.self_encrypt(b'ayuda')

alice_key_cipher = proxy.user_key_cipher(kp)

re_cp = proxy.re_user_cipher(alice_key_cipher, service.get_pubkey())

point = service.decrypt_key_point(re_cp)

print(decrypt(cp, point))
print("=================")
'''
alice = User()
service = Service()

name = b'name:Alicia'
edad = b'age:22'

alice.add_data(b'A')
re_cipher = alice.re_user_cipher(service.get_pubkey())

dec_point = encrypt.el_gamal_decrypt(re_cipher, service.private_key)

dlog = mapping.discrete_log(dec_point)
print(dlog)
print((dlog).to_bytes(1, 'big'))

alice.add_data(b'22')
re_cipher2 = alice.re_user_cipher(service.get_pubkey())

dec_point2 = encrypt.el_gamal_decrypt(re_cipher2, service.private_key)

dlog2 = mapping.discrete_log(dec_point2)
print(dlog2)
print((dlog2).to_bytes(2, 'big'))

print(dlog2-dlog)
print((dlog2-dlog).to_bytes(2, 'big'))
